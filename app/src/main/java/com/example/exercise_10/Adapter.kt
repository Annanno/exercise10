package com.example.exercise_10

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


class Adapter(private val imagesList: List<Int>): RecyclerView.Adapter<Adapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return imagesList.size
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val image = imagesList[position]

        holder.onBind(image)




    }

    inner class ViewHolder(itemview: View): RecyclerView.ViewHolder(itemview){
        val imageView = itemView.findViewById<ImageView>(R.id.image)



        fun onBind(image: Int) {
            imageView.setImageResource(image)
        }



    }


}